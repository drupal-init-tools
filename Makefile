# Packagers may want to override this!
prefix ?= /usr/local

LIBEXEC_DIR ?= $(prefix)/share/drin/libexec
BIN_DIR := $(prefix)/bin
MAN_DIR := $(prefix)/share/man

.PHONY: all local docs installdocs install clean

all: drin

drin: drin.in
	sed -e 's#@libexec@#$(LIBEXEC_DIR)#g' $< > $@ \
	  && chmod +x $@

# Useful during development to run the script from the working directory
local: LIBEXEC_DIR := $(PWD)/libexec
local: clean drin
	touch drin.in

docs: drin.1

installdocs: docs
	install -d $(DESTDIR)$(MAN_DIR)/man1
	install -m644 drin.1 $(DESTDIR)$(MAN_DIR)/man1

install: drin installdocs
	install -d $(DESTDIR)$(LIBEXEC_DIR)
	install -m755 libexec/*.sh $(DESTDIR)$(LIBEXEC_DIR)
	install -d $(DESTDIR)$(BIN_DIR)
	install -m755 $< $(DESTDIR)$(BIN_DIR)

clean:
	rm -f drin drin.1

%.1: %.rst
	rst2man $< > $@
