Helper commands to create and install new Drupal projects.
See the 'drin.rst' file for more info.

To test the commands locally, execute `make local` and run `./drin`.

Dependencies:
  - bash
  - composer
  - git
  - sudo

The shell script template was inspired by
http://agateau.com/2014/template-for-shell-based-command-line-scripts/

Copyright (C) 2017  Antonio Ospite <ao2@ao2.it>
