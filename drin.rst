======
 drin
======

-----------------
Drupal init tools
-----------------

:Author: Antonio Ospite <ao2@ao2.it>
:Date:   2017-06-20
:Copyright: GPLv2+
:Manual section: 1
:Manual group: General Commands Manual

SYNOPSIS
========

*drin* <options> [sub-command]

DESCRIPTION
===========

Helper commands to create and install new Drupal projects.

One problem with setting up a new Drupal project with
drupal-composer/drupal-project is that drush and drupal-console are not
available until the site dependencies have been downloaded, drupal-init-tools
helps to solve this and also adds a nicer command line interface to some
repetitive tasks.

drupal-init-tools commands are especially useful when setting up projects in
user web directories[1].

[1] http://httpd.apache.org/docs/current/howto/public_html.html


OPTIONS
=======

Available options:

  **-h**, **--help**
    this help


Available sub-commands:

  ``new`` `[-h|--help]` `<destdir>` `[composer options (e.g. --devel)]`
    Create a new Drupal project in the `destdir` directory.

  ``bootstrap`` `[--devel|--overwrite-profile|-h|--help]`
    Bootstrap a Drupal project, using settings from a `bootstrap.conf` file.

  ``clean`` `[-h|--help]`
    Cleanup the project, removing all the installed files.

  ``create-profile`` `[-h|--help]` `<title>` `<machine_name>`
    Create an installation profile from the installed project.


EXAMPLES OF USE
===============

Create and install a new Drupal project:

::

  cd ~/public_html
  drin new drupal_test_site
  cd drupal_test_site
  $EDITOR bootstrap.conf
  drin bootstrap --devel


Create an installation profile from the currently installed project:

::

  drin create-profile "Test Profile" test_profile


Clean and rebuild the whole project to verify that installing from scratch works:

::

  drin clean
  drin bootstrap

NOTES ON DATABASE SETUP
=======================

The `bootstrap` command in `drupal-init-tools` uses `drush` for the site
installation, and `drush` requires MySQL super-user access to create new MySQL
users and databases.

On some Linux distributions, like Debian, the MariaDB server is configured by
default to only allow access to the `root` user via the `unix_socket` plugin,
making it unusable by `drush`. In these cases it's recommended to create e new
MySQL super-user.

For password-less access create a MySQL admin user correspondent to the system
user which will execute `drush` (e.g. the current user):

::

  sudo /usr/bin/mysql -e "GRANT ALL ON *.* TO '$USER'@'localhost' IDENTIFIED VIA unix_socket WITH GRANT OPTION"


For password regulated access create a user with a password:

::

  sudo /usr/bin/mysql -e "GRANT ALL ON *.* TO 'USERNAME'@'localhost' IDENTIFIED BY 'password' WITH GRANT OPTION"


For details see also the **PASSWORDS** section in the README.Debian file
provided by the mariadb-server Debian package, either in
`/usr/share/doc/mariadb-server-10.1/README.Debian.gz` or at 
https://salsa.debian.org/mariadb-team/mariadb-10.1/blob/stretch/debian/mariadb-server-10.1.README.Debian#L73

The settings in the `bootstrap.conf` file have to be adjusted according to how
super-user access has been configured.


NOTES ON WEB SERVER SETUP
=========================

The `bootstrap` command in  `drupal-init-tools` supports installing new sites
in *user dirs*; this means that users can have the Drupal sites in their home
directories, usually under the `public_html/` sub-directory.

This may make development more comfortable, as new work can be done in the
usual development environment (git and ssh configuration, editor settings,
etc.).

To configure the Apache web server to enable *user dirs*, first enable the
module:

::

  sudo a2enmod userdir

And then make sure that the configuration is compatible with Drupal, changes
like the following may be necessary, depending on the distribution:

::

  diff --git a/apache2/mods-available/php7.2.conf b/apache2/mods-available/php7.2.conf
  index d4df3e5..0758ea6 100644
  --- a/apache2/mods-available/php7.2.conf
  +++ b/apache2/mods-available/php7.2.conf
  @@ -18,8 +18,8 @@
   # To re-enable PHP in user directories comment the following lines
   # (from <IfModule ...> to </IfModule>.) Do NOT set it to On as it
   # prevents .htaccess files from disabling it.
  -<IfModule mod_userdir.c>
  -    <Directory /home/*/public_html>
  -        php_admin_flag engine Off
  -    </Directory>
  -</IfModule>
  +#<IfModule mod_userdir.c>
  +#    <Directory /home/*/public_html>
  +#        php_admin_flag engine Off
  +#    </Directory>
  +#</IfModule>
  diff --git a/apache2/mods-available/userdir.conf b/apache2/mods-available/userdir.conf
  index 2c334ec..737abdf 100644
  --- a/apache2/mods-available/userdir.conf
  +++ b/apache2/mods-available/userdir.conf
  @@ -3,7 +3,7 @@
          UserDir disabled root

          <Directory /home/*/public_html>
  -		AllowOverride FileInfo AuthConfig Limit Indexes
  +		AllowOverride FileInfo AuthConfig Limit Indexes Options
                  Options MultiViews Indexes SymLinksIfOwnerMatch IncludesNoExec
                  Require method GET POST OPTIONS
          </Directory>

These changes are typically only applied once, after the first installation of
the web server.

SEE ALSO
========

* drupal-composer/drupal-project: https://github.com/drupal-composer/drupal-project
* drush: https://github.com/drush-ops/drush
* drupal-console: https://github.com/hechoendrupal/drupal-console

.. _drupal-composer/drupal-project: https://github.com/drupal-composer/drupal-project
.. _drush: https://github.com/drush-ops/drush
.. _drupal-console: https://github.com/hechoendrupal/drupal-console

BUGS
====

None known.
