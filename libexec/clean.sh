#!/bin/bash
# Clean up a Drupal project directory
#
# Copyright (C) 2017  Antonio Ospite <ao2@ao2.it>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

set -e

usage() {
  cat <<EOF
usage: drin $(basename "$0" .sh) [-h|--help]

Cleanup the project, removing all the installed files.

Options:
  -h, --help          display this usage message and exit
EOF
}

while [ $# -gt 0 ];
do
  case "$1" in
    -h|--help)
      usage
      exit 0
      ;;
    -*)
      echo "Error: Unknown option '${1}'" 1>&2
      ;;
  esac
  shift
done

[ -f "bootstrap.conf" ] || { echo "Aborting, run this command from the Drupal project directory." 1>&2; exit 1; }

CONFIRMATION_STRING="YESIAMSURE"

echo "WARNING! This removes any files in the config/ web/ and vendor/ directories."
echo "Are you sure you want to continue?"
echo
read -r -p "Type ${CONFIRMATION_STRING} to confirm: " INPUT

run() {
  echo "$@"
  "$@"
}

[ "$INPUT" = "$CONFIRMATION_STRING" ] && run sudo --reset-timestamp rm -rf config/ web/ vendor/ composer.lock
