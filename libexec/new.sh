#!/bin/bash
# Create a new Drupal project
#
# Copyright (C) 2017  Antonio Ospite <ao2@ao2.it>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

set -e

usage() {
  cat <<EOF
usage: drin $(basename "$0" .sh) [-h|--help] <destdir>

Create a new Drupal project in the 'destdir' directory.

Options:
  -h, --help          display this usage message and exit
EOF
}

while [ $# -gt 0 ];
do
  case "$1" in
    -h|--help)
      usage
      exit 0
      ;;
    -*)
      echo "Error: Unknown option '${1}'" 1>&2
      ;;
    *)
      break
      ;;
  esac
  shift
done

[ "x$1" = "x" ] && { usage 1>&2; exit 1; }

[ -d "$1" ] && { echo "Aborting, project directory already exists." 1>&2; exit 1; }

DESTDIR="$1"

command -v composer &> /dev/null || { echo "Aborting, 'composer' not available." 1>&2; exit 1; }
command -v git &> /dev/null || { echo "Aborting, 'git' not available." 1>&2; exit 1; }

# Create a new project keeping the VCS metadata so it's easier to bring in
# updates to drupal-composer/drupal-project itself.
echo "Creating a new Drupal project..."
composer create-project drupal-composer/drupal-project:8.x-dev@dev "$DESTDIR" --keep-vcs --stability dev --no-interaction

pushd "$DESTDIR" > /dev/null

git remote rename origin upstream
git checkout -b master

# Add some patches
composer config extra.patches-file "composer.patches.json"

cat > composer.patches.json <<EOF
{
  "patches": {
    "drupal/core": {
      "drupal-do_not_disable_MultiViews_htaccess": "https://www.drupal.org/files/issues/drupal-do_not_disable_MultiViews_htaccess-2619250-24.patch"
    }
  }
}
EOF

# Apply the patches
composer update

# Add a template config file for the bootstrap script
cat > bootstrap.conf <<EOF
#DB_NAME="drupal_test_database"
#DB_USER="drupal_test_user"
#DB_PASS="drupal_test_password"

#ACCOUNT_NAME="admin"
#ACCOUNT_PASS="admin"
#ACCOUNT_MAIL="admin@example.com"

#SITE_NAME="example"
#SITE_MAIL="admin@example.com"

#SITE_BASE_PATH="/~${USER}/$(basename "${PWD}")/web"

#WEB_SERVER_GROUP="www-data"

#INSTALLATION_PROFILE="standard"

#TRUSTED_HOSTS=(
#  "localhost"
#  "ip4-localhost"
#  "ip6-localhost"
#)

# See the 'drin' man page for details about database access.

# Set MYSQL_PASSWORDLESS_ACCESS to 'true' if the database is configured to
# allow administrative password-less access to the user who will execute the
# 'drin boostrap' command.
#MYSQL_PASSWORDLESS_ACCESS=true

# If, instead,  administrative access requires a password, uncomment and
# change the values of the following variables.
#MYSQL_SU_USER='root'
#MYSQL_SU_PASSWORD='password'
EOF

cat >> .gitignore <<EOF

# Ignore the configuration for the bootstrap script
bootstrap.conf
EOF

echo
echo "Uncomment and customize the values in the bootstrap.conf file."

# Add some basic access control to protect sensitive files like bootstrap.conf
cat > .htaccess <<EOF
Options -Indexes
SetEnvIf Request_URI "/web(/.*)?$" access_granted
Order allow,deny
Allow from env=access_granted
EOF

echo
echo "Double check the provided .htaccess file to make sure that access to"
echo "'$PWD' is restricted by the web server."

popd > /dev/null
